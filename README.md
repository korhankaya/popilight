* Synchronized playing of multi channel audio across the mobile devices. Each channel can contain a different instrument or group of instruments. Can be used with Bluetooth or internal speakers.

* Displaying synchronized multi angle/multi view videos across the mobile devices.

• Any 2D or 3D Matrices of video displays can be implemented using Popilight
Platform.

• A Sequence for vibration and flash for playing simultaneously with the video or show content.

• Can be part of home entertaintment system, While playing a movie from Smart TV, alternative view-port/frame of movie can be watched in synch from the tablet and phone simultaneously.